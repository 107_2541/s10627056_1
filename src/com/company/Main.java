package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int enternumber;
        Scanner input = new Scanner(System.in);
        System.out.println("請輸入一個數,判斷其為奇數或偶數:");
        enternumber = input.nextInt();
        if((enternumber & 1)==0)
            System.out.printf("%d為偶數...",enternumber);
        else System.out.printf("%d為奇數...",enternumber);
    }
}
